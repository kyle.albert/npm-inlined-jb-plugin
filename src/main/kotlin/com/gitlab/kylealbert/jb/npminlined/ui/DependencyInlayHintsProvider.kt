package com.gitlab.kylealbert.jb.npminlined.ui

import com.gitlab.kylealbert.jb.npminlined.ui.hints.PackageJsonDepHintsPresentationFactory
import com.gitlab.kylealbert.jb.npminlined.util.SettingsUtil
import com.intellij.codeInsight.hints.*
import com.intellij.icons.AllIcons
import com.intellij.json.JsonLanguage
import com.intellij.json.psi.JsonProperty
import com.intellij.json.psi.JsonStringLiteral
import com.intellij.json.psi.JsonValue
import com.intellij.json.psi.impl.JsonPsiImplUtils
import com.intellij.lang.Language
import com.intellij.lang.javascript.buildTools.npm.PackageJsonUtil
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.DumbService
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiFile
import com.intellij.psi.util.PsiTreeUtil
import com.intellij.ui.components.CheckBox
import com.intellij.ui.layout.panel
import com.intellij.util.PsiNavigateUtil
import com.intellij.util.ui.JBUI
import org.jetbrains.annotations.Nls
import javax.swing.JPanel

class DependencyInlayHintsProvider : InlayHintsProvider<DependencyInlayHintsProvider.Settings> {
	data class Settings(
		var showForPinnedVersions: Boolean = false
	)

	companion object {
		val SUPPORTED_TOP_LEVEL_PACKAGE_JSON_PROPERTIES: Set<String> = setOf(
			PackageJsonUtil.DEPENDENCIES,
			PackageJsonUtil.DEV_DEPENDENCIES,
			"peerDependencies"
		)
		val KEY: SettingsKey<Settings> = SettingsKey(SettingsUtil.settingKeyId("json.package.dependency.inlay"))
	}

	override val key: SettingsKey<Settings>
		get() = KEY

	override val name: String
		@Nls(capitalization = Nls.Capitalization.Sentence)
		get() = "package.json hints"

	override val previewText: String?
		get() = """
			|{
			|   "dependencies": {
			|       "minor": "^1.2.3",
			|       "patch": "~1.4.5",
			|       "pinned": "2.6.7"
			|   },
			|   "devDependencies": {
			|       "dev-minor": "^1.2.3",
			|       "dev-patch": "~1.4.5",
			|       "dev-pinned": "2.6.7"
			|   },
			|   "peerDependencies": {
			|       "minor": "^1.2.3",
			|       "patch": "~1.4.5",
			|       "pinned": "2.6.7"
			|   },
			|}
		""".trimMargin()

	override fun createConfigurable(settings: Settings): ImmediateConfigurable = object : ImmediateConfigurable {
		val showForPinnedVersions = "Show for pinned versions"

		private val showForPinnedVersionsField = CheckBox(showForPinnedVersions)

		override fun createComponent(listener: ChangeListener): JPanel {
			showForPinnedVersionsField.isSelected = settings.showForPinnedVersions
			showForPinnedVersionsField.addItemListener { handleChange(listener) }

			val panel = panel {
				row { showForPinnedVersionsField(pushX) }
			}
			panel.border = JBUI.Borders.empty(5)
			return panel
		}

		private fun handleChange(listener: ChangeListener) {
			settings.showForPinnedVersions = showForPinnedVersionsField.isSelected
			listener.settingsChanged()
		}
	}

	override fun createSettings(): Settings = Settings()

	override fun getCollectorFor(file: PsiFile, editor: Editor, settings: Settings, sink: InlayHintsSink): InlayHintsCollector? =
		object : FactoryInlayHintsCollector(editor) {
			val hintFactory = PackageJsonDepHintsPresentationFactory(factory)
			val isPackageJsonFile = PackageJsonUtil.isPackageJsonFile(file)

			override fun collect(element: PsiElement, editor: Editor, sink: InlayHintsSink) : Boolean {
				if (
					file.project.service<DumbService>().isDumb
					|| !isPackageJsonFile
					|| element !is JsonProperty
					|| element.value !is JsonStringLiteral
					|| PackageJsonUtil.isTopLevelProperty(element)
				) {
					return true
				}
				val topLevelProperty = PackageJsonUtil.findContainingTopLevelProperty(element)
					?: return true
				if (!SUPPORTED_TOP_LEVEL_PACKAGE_JSON_PROPERTIES.contains(topLevelProperty.name)) {
					return true
				}

				var version = JsonPsiImplUtils.getValue(element.value as JsonStringLiteral)
				// TODO regexp check the version, create a service to run `npm outdated --json` or yarn equivalent
				val presentation = hintFactory.versionUpdateHint(element.name, version)
				sink.addInlineElement(element.textOffset + element.textLength, true, presentation)

				return true
			}
		}

	override fun isLanguageSupported(language: Language): Boolean {
		return language.isKindOf(JsonLanguage.INSTANCE)
	}
}
