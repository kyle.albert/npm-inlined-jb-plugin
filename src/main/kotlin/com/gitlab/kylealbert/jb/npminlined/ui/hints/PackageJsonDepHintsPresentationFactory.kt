package com.gitlab.kylealbert.jb.npminlined.ui.hints

import com.intellij.codeInsight.hints.presentation.InlayPresentation
import com.intellij.codeInsight.hints.presentation.PresentationFactory

class PackageJsonDepHintsPresentationFactory(private val factory: PresentationFactory) {
	fun versionUpdateHint(packageName: String, currentPackageVersion: String): InlayPresentation = factory.roundWithBackground(
		listOf(text(": $packageName $currentPackageVersion")).join()
	)

	private fun checkSize(level: Int, elementsCount: Int): Boolean =
		level + elementsCount > FOLDING_THRESHOLD

	private fun List<InlayPresentation>.join(separator: String = ""): InlayPresentation {
		if (separator.isEmpty()) {
			return factory.seq(*toTypedArray())
		}
		val presentations = mutableListOf<InlayPresentation>()
		var first = true
		for (presentation in this) {
			if (!first) {
				presentations.add(text(separator))
			}
			presentations.add(presentation)
			first = false
		}
		return factory.seq(*presentations.toTypedArray())
	}

	private fun text(text: String?): InlayPresentation = factory.smallText(text ?: "?")

	companion object {
		private const val FOLDING_THRESHOLD: Int = 3
	}
}
