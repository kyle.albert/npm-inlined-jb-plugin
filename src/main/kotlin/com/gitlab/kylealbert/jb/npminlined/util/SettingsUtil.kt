package com.gitlab.kylealbert.jb.npminlined.util

import com.gitlab.kylealbert.jb.npminlined.NpmInlined
import com.intellij.codeInsight.hints.SettingsKey

object SettingsUtil {
    val SETTINGS_KEY_PREFIX = "${NpmInlined.PLUGIN_ID}."
    fun settingKeyId(id: String): String = "$SETTINGS_KEY_PREFIX$id"
}
