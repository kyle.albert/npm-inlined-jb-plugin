import org.gradle.api.JavaVersion.VERSION_1_8
import org.gradle.api.internal.HasConvention
import org.jetbrains.intellij.tasks.RunIdeTask
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
	repositories {
		mavenCentral()
		maven("https://jitpack.io")
	}
	dependencies {
		classpath(kotlin("gradle-plugin", version = "1.3.50"))
	}
}

plugins {
	kotlin("jvm") version "1.3.50"
	id("org.jetbrains.intellij") version "0.4.10"
}

repositories {
	mavenCentral()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		jvmTarget = "1.8"
		languageVersion = "1.3"
		apiVersion = "1.3"
		freeCompilerArgs = listOf("-Xjvm-default=enable")
	}
}

configure<JavaPluginConvention> {
	sourceCompatibility = VERSION_1_8
	targetCompatibility = VERSION_1_8
}

sourceSets {
	main {
		kotlin.srcDirs("src/main/kotlin")
		resources.srcDirs("src/main/resources")
	}
//	test {
//		kotlin.srcDirs("src/test/kotlin")
//		resources.srcDirs("src/test/resources")
//	}
}

// region IntelliJ Gradle Plugin

val ijVersion = "IU-192.6817.14"
val ijPluginName = "NPM Inlined"

// See https://github.com/JetBrains/gradle-intellij-plugin/
intellij {
	version = ijVersion
    type = "IU"
	downloadSources = true
    updateSinceUntilBuild = false
	instrumentCode = true
    pluginName = ijPluginName
    ideaDependencyCachePath = project.buildDir.absolutePath
	setPlugins("IntelliLang", "JavaScriptLanguage", "NodeJS")
}

tasks.withType<RunIdeTask> {
	// Default args for IDEA installation
	jvmArgs("-Xmx1G", "-XX:+UseConcMarkSweepGC", "-XX:SoftRefLRUPolicyMSPerMB=50", "--add-exports=java.base/jdk.internal.vm=ALL-UNNAMED")
	// uncomment if `unexpected exception ProcessCanceledException` prevents you from debugging a running IDE
	// jvmArgs("-Didea.ProcessCanceledException=disabled")
}

// endregion

// region Plugin-specific

group = "com.gitlab.kylealbert.jb"
version = "1.0.0-SNAPSHOT"

//patchPluginXml {
//	changeNotes """
//      Add change notes here.<br>
//      <em>most HTML tags may be used</em>"""
//}

dependencies {
	testCompile(group = "junit", name = "junit", version = "4.12")
}

// endregion

// region Util

inline operator fun <T : Task> T.invoke(a: T.() -> Unit): T = apply(a)

val SourceSet.kotlin: SourceDirectorySet
	get() =
		(this as HasConvention)
			.convention
			.getPlugin(KotlinSourceSet::class.java)
			.kotlin

fun SourceSet.kotlin(action: SourceDirectorySet.() -> Unit) =
	kotlin.action()
// endregion
